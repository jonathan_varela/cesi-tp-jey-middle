package fr.cesi.tpjeymiddle.service;

import fr.cesi.tpjeymiddle.domain.PaymentOrderEntity;
import fr.cesi.tpjeymiddle.dto.PaymentOrderDTO;
import fr.cesi.tpjeymiddle.mapper.PaymentOrderMapper;
import fr.cesi.tpjeymiddle.repository.PaymentOrderRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PaymentOrderServiceTest {

  @MockBean private PaymentOrderRepository paymentOrderRepository;

  @MockBean private JmsTemplate jmsTemplate;

  @Autowired private PaymentOrderService paymentOrderService;

  private List<PaymentOrderEntity> mockedEntites;

  private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

  @Before
  public void setup() throws ParseException {
    mockPaymentEntities();
    doReturn(this.mockedEntites.get(0)).when(this.paymentOrderRepository).save(any(PaymentOrderEntity.class));
    doReturn(this.mockedEntites).when(this.paymentOrderRepository).findAll();
    doNothing().when(jmsTemplate).convertAndSend(any(String.class), any(String.class));
  }

  @Test
  public void shouldReturnAllPaymentOrderSortedByCreationDate() {
    // When
    List<PaymentOrderEntity> allPaymentOrder = this.paymentOrderService.getAllPaymentOrder();

    // Then
    assertThat(allPaymentOrder.size() == 2).isTrue();
    assertThat(this.sdf.format(allPaymentOrder.get(0).getCreationDate())).isEqualTo("29/03/1990 07:30:15");
  }

  /** populate mockedEntites */
  public void mockPaymentEntities() throws ParseException {
      this.mockedEntites =
        Arrays.asList(
            PaymentOrderEntity.builder()
                .amount(22)
                .creationDate(sdf.parse("29/03/1990 07:30:15"))
                .customerName("customer_name1")
                .build(),
            PaymentOrderEntity.builder()
                .amount(10)
                .creationDate(new Date())
                .customerName("customer_name2")
                .build());
  }
}
