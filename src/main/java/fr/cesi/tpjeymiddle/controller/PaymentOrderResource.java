package fr.cesi.tpjeymiddle.controller;

import fr.cesi.tpjeymiddle.dto.PaymentOrderDTO;
import fr.cesi.tpjeymiddle.domain.PaymentOrderEntity;
import fr.cesi.tpjeymiddle.exception.OrderProcessingException;
import fr.cesi.tpjeymiddle.service.PaymentOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api")
@Api(value = "paymentorder")
public class PaymentOrderResource {

  @Autowired private PaymentOrderService paymentOrderService;

  @PostMapping("/payment-orders")
  @ApiOperation(value = "Create a payment order")
  public ResponseEntity<PaymentOrderEntity> savePayementOrder(
      @Valid @RequestBody PaymentOrderDTO paymentOrderDTO) throws OrderProcessingException {
    PaymentOrderEntity paymentOrderEntity = paymentOrderService.savePayment(paymentOrderDTO);
    return ResponseEntity.ok().body(paymentOrderEntity);
  }

  @GetMapping("/payment-orders")
  @ApiOperation(value = "Return a list of payment order sorted by creation's date")
  public ResponseEntity<List<PaymentOrderEntity>> getPayementOrders() {
    return ResponseEntity.ok().body(paymentOrderService.getAllPaymentOrder());
  }
}
