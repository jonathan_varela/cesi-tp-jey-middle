package fr.cesi.tpjeymiddle.repository;

import fr.cesi.tpjeymiddle.domain.PaymentOrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface PaymentOrderRepository extends JpaRepository<PaymentOrderEntity, Long> {
}
