package fr.cesi.tpjeymiddle.mapper;

import fr.cesi.tpjeymiddle.domain.PaymentOrderEntity;
import fr.cesi.tpjeymiddle.domain.PaymentOrderEntity.PaymentOrderEntityBuilder;
import fr.cesi.tpjeymiddle.dto.PaymentOrderDTO;
import fr.cesi.tpjeymiddle.dto.PaymentOrderDTO.PaymentOrderDTOBuilder;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-05-15T09:14:25+0200",
    comments = "version: 1.3.0.Beta2, compiler: javac, environment: Java 1.8.0_144 (Oracle Corporation)"
)
public class PaymentOrderMapperImpl implements PaymentOrderMapper {

    @Override
    public PaymentOrderEntity paymentDtoToPaymentEntity(PaymentOrderDTO paymentOrderDTO) {
        if ( paymentOrderDTO == null ) {
            return null;
        }

        PaymentOrderEntityBuilder paymentOrderEntity = PaymentOrderEntity.builder();

        paymentOrderEntity.customerName( paymentOrderDTO.getCustomerName() );
        paymentOrderEntity.amount( paymentOrderDTO.getAmount() );
        paymentOrderEntity.creationDate( paymentOrderDTO.getCreationDate() );

        return paymentOrderEntity.build();
    }

    @Override
    public PaymentOrderDTO paymentEntityToPaymentDto(PaymentOrderEntity paymentOrderEntity) {
        if ( paymentOrderEntity == null ) {
            return null;
        }

        PaymentOrderDTOBuilder paymentOrderDTO = PaymentOrderDTO.builder();

        paymentOrderDTO.amount( paymentOrderEntity.getAmount() );
        paymentOrderDTO.creationDate( paymentOrderEntity.getCreationDate() );
        paymentOrderDTO.customerName( paymentOrderEntity.getCustomerName() );

        return paymentOrderDTO.build();
    }
}
