package fr.cesi.tpjeymiddle.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.cesi.tpjeymiddle.domain.PaymentOrderEntity;
import fr.cesi.tpjeymiddle.dto.PaymentOrderDTO;
import fr.cesi.tpjeymiddle.exception.OrderProcessingException;
import fr.cesi.tpjeymiddle.mapper.PaymentOrderMapper;
import fr.cesi.tpjeymiddle.repository.PaymentOrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class PaymentOrderService {

  @Autowired private PaymentOrderRepository paymentOrderRepository;

  @Autowired private JmsTemplate jmsTemplate;

  @Value("${message-broker.topic-name}")
  private String topicName;

  /**
   * Get all payment from H2 mapped as PayementDTO ordered by creationDate
   *
   * @return
   */
  public List<PaymentOrderEntity> getAllPaymentOrder() {
    return paymentOrderRepository
        .findAll()
        .parallelStream()
        .sorted(Comparator.comparing(PaymentOrderEntity::getCreationDate))
        .collect(Collectors.toList());
  } // getAllPaymentOrder()

  /**
   * Save payment to H2 and put him as a message into ActiveMQ queue
   *
   * @param paymentOrderDTO
   */
  public PaymentOrderEntity savePayment(PaymentOrderDTO paymentOrderDTO)
      throws OrderProcessingException {

    // if paymentOrderDTO is null then return null
    if (paymentOrderDTO == null) {
      return null;
    }

    // Map PaymentOrderDTO to PaymentOrderEntity
    PaymentOrderEntity paymentOrderEntity =
        Mappers.getMapper(PaymentOrderMapper.class).paymentDtoToPaymentEntity(paymentOrderDTO);

    // save entity and flush for return id
    paymentOrderRepository.saveAndFlush(paymentOrderEntity);

    // Add paymentOrderEntity into activeMQ queue
    try {
      String payload = new ObjectMapper().writeValueAsString(paymentOrderEntity);
      log.info(String.format("sending payload='%s'", payload));
      jmsTemplate.convertAndSend(topicName, payload);
      // Return payementOrderEntity
    } catch (JsonProcessingException e) {
      throw new OrderProcessingException(e.getMessage());
    }
    return paymentOrderEntity;
  } // savePayment(PaymentOrderDTO paymentOrderDTO)
}
