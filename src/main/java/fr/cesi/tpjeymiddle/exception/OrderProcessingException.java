package fr.cesi.tpjeymiddle.exception;

public class OrderProcessingException extends Exception {
    public OrderProcessingException(String msg){
        super(msg);
    }
}
