package fr.cesi.tpjeymiddle.controller;

import fr.cesi.tpjeymiddle.domain.PaymentOrderEntity;
import fr.cesi.tpjeymiddle.dto.PaymentOrderDTO;
import fr.cesi.tpjeymiddle.exception.OrderProcessingException;
import fr.cesi.tpjeymiddle.mapper.PaymentOrderMapper;
import fr.cesi.tpjeymiddle.service.PaymentOrderService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PaymentOrderResourceTest {

  @MockBean private PaymentOrderService paymentOrderService;

  @Autowired private TestRestTemplate restTemplate;

  private List<PaymentOrderEntity> mockedEntites;

  @Before
  public void init() throws OrderProcessingException {
    // populate mockedEntites
    mockPaymentEntities();

    // Mock paymentOrderService savePayment and getAllPaymentOrder methods
    doReturn(mockedEntites.get(0)).when(paymentOrderService).savePayment(any(PaymentOrderDTO.class));

    doReturn(mockedEntites).when(paymentOrderService).getAllPaymentOrder();
  }

  @Test
  public void shouldSavePaymentOrder() {

    // when
    ResponseEntity<PaymentOrderEntity> response =
        restTemplate.postForEntity(
            "/api/payment-orders", mockedEntites.get(0), PaymentOrderEntity.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(
        response.getBody().getCustomerName().equals(mockedEntites.get(0).getCustomerName()));
  }

  @Test
  public void savePaymentOrderWithInvalidAmount() {

    // given
    PaymentOrderDTO paymentOrderDTO =
        PaymentOrderDTO.builder()
            .amount(-1)
            .creationDate(new Date())
            .customerName("customer_name1")
            .build();

    // when
    ResponseEntity<PaymentOrderEntity> response =
        restTemplate.postForEntity(
            "/api/payment-orders", paymentOrderDTO, PaymentOrderEntity.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void savePaymentOrderWithInvalidCustomerName() {

    // given
    PaymentOrderDTO paymentOrderDTO =
        PaymentOrderDTO.builder()
            .amount(-1)
            .creationDate(new Date())
            .customerName("")
            .build();

    // when
    ResponseEntity<PaymentOrderEntity> response =
        restTemplate.postForEntity(
            "/api/payment-orders", paymentOrderDTO, PaymentOrderEntity.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void shouldReturnPaymentOrderList() {

    // when
    ResponseEntity<List<PaymentOrderDTO>> response =
        restTemplate.exchange(
            "/api/payment-orders",
            HttpMethod.GET,
            null,
            new ParameterizedTypeReference<List<PaymentOrderDTO>>() {});

    // then
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody().size() == 2);
  }

  /** populate mockedEntites */
  public void mockPaymentEntities() {
    mockedEntites =
        Arrays.asList(
            PaymentOrderEntity.builder()
                .amount(22)
                .creationDate(new Date())
                .customerName("customer_name1")
                .build(),
            PaymentOrderEntity.builder()
                .amount(10)
                .creationDate(new Date())
                .customerName("customer_name2")
                .build());
  }
}
