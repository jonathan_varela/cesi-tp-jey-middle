package fr.cesi.tpjeymiddle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

@EnableJms
@SpringBootApplication
public class TpJeyMiddleApplication {

    public static void main(String[] args) {
        SpringApplication.run(TpJeyMiddleApplication.class, args);
    }
}
