package fr.cesi.tpjeymiddle.mapper;

import fr.cesi.tpjeymiddle.domain.PaymentOrderEntity;
import fr.cesi.tpjeymiddle.dto.PaymentOrderDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PaymentOrderMapper {
  PaymentOrderMapper INSTANCE = Mappers.getMapper(PaymentOrderMapper.class);

  PaymentOrderEntity paymentDtoToPaymentEntity(PaymentOrderDTO paymentOrderDTO);

  PaymentOrderDTO paymentEntityToPaymentDto(PaymentOrderEntity paymentOrderEntity);
}
